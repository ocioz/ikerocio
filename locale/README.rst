Translations
============

Translations will be placed in this folder when running::

    django-admin makemessages --locale=<locale>

Then, translations must be compiled when running::

    django-admin compilemessages --locale=<locale>
